import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:glassmorphism_ui/glassmorphism_ui.dart';
import 'package:login_using_firebase/constants/asset_image.dart';
import 'package:login_using_firebase/services/controller/widgetController/themeController.dart';

Widget scaffoldReady({required BuildContext context, required Widget body}) {
  return GestureDetector(
    onTap: () {
      FocusScope.of(context).unfocus();
    },
    child: Stack(
      children: [
        GetBuilder<ThemeController>(
          builder: (controller) {
            return GlassImage(
              height: double.infinity,
              width: double.infinity,
              blur: 20,
              image: controller.isDark
                  ? Image.asset(
                      AssetImages.backgroundImg,
                      fit: BoxFit.cover,
                    )
                  : Image.asset(
                      AssetImages.backgroundImgLight,
                      fit: BoxFit.cover,
                    ),
              overlayColor: controller.isDark
                  ? Colors.deepPurple.withOpacity(0.9)
                  : Colors.deepPurple.withOpacity(0.5),
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Colors.purple.withOpacity(0.8),
                  Colors.blue.withOpacity(0.1),
                ],
              ),
              border: const Border.fromBorderSide(BorderSide.none),
              shadowStrength: 5,
              borderRadius: BorderRadius.circular(16),
              shadowColor: Colors.white.withOpacity(0.24),
            );
          },
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: body,
        ),
      ],
    ),
  );
}
